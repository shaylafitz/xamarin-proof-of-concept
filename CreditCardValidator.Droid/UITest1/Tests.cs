﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;
using Xamarin.UITest.Android;

namespace UITest1
{
    [TestFixture]
    public class Tests
    {
        AndroidApp app;

        [SetUp]
        public void BeforeEachTest()
        {
            // TODO: If the Android app being tested is included in the solution then open
            // the Unit Tests window, right click Test Apps, select Add App Project
            // and select the app projects that should be tested.
            //app = ConfigureApp.Android.StartApp();
            app = ConfigureApp.Android
            .ApkFile(@"C: \Users\shayla.fitzpatrick\Downloads\CreditCardValidator.Droid\CreditCardValidator.Droid\bin\Release\com.xamarin.example.creditcardvalidator.apk")
            .PreferIdeSettings()
            .EnableLocalScreenshots()
            .StartApp();
        }

        [Test]
        public void CardNumberTooShort()
        {
           // app.Repl();
            app.Screenshot("First screen.");

            app.WaitForElement(c => c.Marked("action_bar_title").Text("Enter Credit Card Number"));
            app.EnterText(c => c.Marked("creditCardNumberText"), new string('9', 15));

            app.Screenshot("Enter too few characters.");

            app.Tap(c => c.Marked("validateButton"));
            app.WaitForElement(c => c.Marked("errorMessagesText").Text("Credit card number is too short."));
            app.Screenshot("get error message");
        }
    }
}

